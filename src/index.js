
export class ZCore {
    //#region properties

    #logger;
    #httpFramework;
    #modules;

    //#endregion

    //#region enum

    static HTTP_FRAMEWORK = {
        EXPRESS: 0,
    };

    //#endregion

    constructor() {
    }
    
    //#region getters

    get logger() {
        return this.#logger();
    }

    set logger(value) {
        this.#logger = value;
        return this;
    }

    get httpFramework() {
        return this.#httpFramework();
    }

    set httpFramework(value) {
        this.#httpFramework = value;
        return this;
    }

    get modules() {
        return this.#modules();
    }

    //#endregion

    load(options){

    }
}